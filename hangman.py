from turtle import *

def guess_check(str, letter):
  places = []
  start = 0
  while True:
    where = str.find(letter, start)
    if (where == -1):
      break
    places.append(where)
    start = where + 1
  return places

def drawman(losses):

  if losses == 1:
    print("One strike.")
    penup()
    forward(20)
    pendown()
    left(90)
    circle(10)
    right(90)
  if losses == 2:
    print("Two strikes")
    forward(30)
  if losses == 3: 
    print("Three Strikes")
    right(100)
    forward(20)
    back(20)
  if losses == 4:
    print("Four strikes")
    left(200)
    forward(20)
    back(20)
  if losses == 5:
    print("Five strikes")
    right(100)
    forward(24)
    left(25)
    forward(28)
    back(28)
  if losses == 6:
    print("Six Strikes")
    right(50)
    forward(28)
    back(28)
    penup()
    forward(50)
    pendown()
    write("YOU LOST")
    done()

word = "SOLVED"

solution = []
losscounter = 0

for i in word:
  solution.append(i)

location = []
guessing = []
start = 0

for i in word:
  guessing.append("_")

game_solve = False

while game_solve == False:

  for i in guessing:
    print(i, end=" ")

  print("")
  guess = input("What letter would you like to guess? ")
  guess = guess.upper()
  locations = guess_check(word, guess)

  if locations == []:
    print("Wrong")

    if losscounter == 0:
      forward(200)
      back(100)
      left(90)
      forward(100)
      left(90)
      forward(70)
      left(90)
      forward(20)
      left(90)
      right(90)

    losscounter += 1
    drawman(losscounter)
    
    # INSERT TURTLE HERE
  else:
    for i in locations:
      guessing[i] = guess
  
  if losscounter == 6:
    print("YOU LOST.")
    break
  if guessing == solution:
    print("YOU WON!")
    break
